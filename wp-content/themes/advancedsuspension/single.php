<?php
 
/**
 * Template Name: Single Post
 */

add_filter( 'body_class', 'mc_body_class' );
function mc_body_class( $classes ) {

    $classes[] = 'blog';
    return $classes;

}


add_action('genesis_before_loop','mc_do_search_form');
function mc_do_search_form(){
    get_search_form();
}

//* Add features image on single post

add_action( 'genesis_entry_header', 'single_post_featured_image', 15 );

function single_post_featured_image() {

    if ( ! is_singular( 'post' ) )
        return;

        $img = genesis_get_image( array( 'format' => 'html', 'size' => 'single-featured-image', 'attr' => array( 'class' => 'post-image entry-image img-responsive' ) ) );
        printf( '<a class="col-md-12 featured-img-link" href="%s" title="%s">%s</a>', get_permalink(), the_title_attribute( 'echo=0' ), $img );

}



//Loop
remove_action('genesis_loop', 'genesis_do_loop'); // Remove the standard loop
add_action('genesis_loop', 'mac_do_loop'); //Use Custom Loop under includes/structures/_loops.php

//Post Title
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
//add_action( 'genesis_entry_content', 'genesis_do_post_title',9 );

//Post Info
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
add_action('genesis_entry_header','mac_post_info',12);

//add entry content custom classes
add_filter('mc_entry_content_class','do_entry_content_class');
function do_entry_content_class(){
    return 'entry-content col-md-12';
}

//Modify Read More Link
add_filter( 'get_the_content_more_link', 'mc_read_more_link' );
function mc_read_more_link() {
    return '<br><a class="more-link btn btn-warning" href="' . get_permalink() . '">Read More</a>';
}

//Remove Post Meta on Entry Footer
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

genesis();