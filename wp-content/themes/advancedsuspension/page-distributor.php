<?php
/*
Template Name: Distributors Page
*/

add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_loop', 'okimangelo_page_loop' );

function okimangelo_page_loop() { ?>

	<section class="row distributors">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php if(has_post_thumbnail()) { ?>
				<?php the_post_thumbnail('full',array('class'=>"img-responsive")); ?>
			<?php }
            //reset default query for page contents
//            wp_reset_postdata();
            wp_reset_query();
            ?>
			<section class="row">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
					<?php $big_title = get_field('big_title'); if(!empty($big_title)) { ?>
						<h1 class="big-title"><?php echo $big_title; ?></h1>
					<?php }
    ?>
                </div>
            </section>
            <section class="row distributor-content">
                <?php
                //Query Danmark
                $args = array(
                    'post_type' => 'distributors',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'distributors_types',
                            'field' => 'slug',
                            'terms' => array( 'danmark' )
                        )
                    )
                );

                $queryOne = new WP_Query($args);

                // The Danmark Loop
                    if ( $queryOne->have_posts() ) : ?>
                        <aside class="col-md-4">
                            <a id="danmark" href="#" class="distributor_link active"><h2 class="skew-bg col-md-12"><span class="text-center col-md-12">Danmark</span></h2></a>
                            <ul class="distributor-content col-md-12 list-unstyled">
                            <?php
                            while ( $queryOne->have_posts() ) : $queryOne->the_post(); ?>
                                        <li><?php the_content(); ?></li>
                                    <?php endwhile;
                                wp_reset_postdata(); ?>
                            </ul>
                        </aside>
                        <?php endif; ?>

                <?php
                //Query Norge
                $args = array(
                    'post_type' => 'distributors',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'distributors_types',
                            'field' => 'slug',
                            'terms' => array( 'norge' )
                        )
                    )
                );

                $queryOne = new WP_Query($args);

                // The Norge Loop
                if ( $queryOne->have_posts() ) : ?>
                    <aside class="col-md-4">
                        <a id="norge" href="#" class="distributor_link"><h2 class="skew-bg col-md-12"><span class="text-center col-md-12">Norge</span></h2></a>
                        <ul class="distributor-content col-md-12 list-unstyled hidden">
                            <?php
                            while ( $queryOne->have_posts() ) : $queryOne->the_post(); ?>
                                <li><?php the_content(); ?></li>
                            <?php endwhile;
                            wp_reset_postdata(); ?>
                        </ul>
                    </aside>
                <?php endif; ?>

                <?php
                //Query Sverige
                $args = array(
                    'post_type' => 'distributors',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'distributors_types',
                            'field' => 'slug',
                            'terms' => array( 'sverige' )
                        )
                    )
                );

                $queryOne = new WP_Query($args);

                // The Sverige Loop
                if ( $queryOne->have_posts() ) : ?>
                    <aside class="col-md-4">
                        <a id="sverige" href="#" class="distributor_link"><h2 class="skew-bg col-md-12"><span class="text-center col-md-12">Sverige</span>
                            </h2></a>
                        <ul class="distributor-content col-md-12 list-unstyled hidden">
                            <?php
                            while ( $queryOne->have_posts() ) : $queryOne->the_post(); ?>
                                <li><?php the_content(); ?></li>
                            <?php endwhile;
                            wp_reset_postdata(); ?>
                        </ul>
                    </aside>
                <?php endif; ?>
            </section>






		</div>
	</section>

<?php }

add_action('wp_footer','distributor_scripts');
function distributor_scripts(){
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.distributor_link').click(function(){
                //store clicked link to variable
                var activeLink = $(this);
                //add hidden class to all distributor content
                $('ul.distributor-content').addClass('hidden');
                $('.distributor_link').removeClass('active');
                //Add active class to the Clicked Link and remove hidden class to the active content
                activeLink.addClass('active');
                activeLink.siblings().removeClass('hidden');

                return false;
            });
        });
    </script>
<?php
}

genesis();

