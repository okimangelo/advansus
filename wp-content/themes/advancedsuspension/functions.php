<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

// Localization
load_child_theme_textdomain(  'okimangelo', apply_filters(  'child_theme_textdomain', get_stylesheet_directory(  ) . '/languages', 'okimangelo'  )  );

// include Advanced Custom Fields to the theme
include_once( CHILD_DIR . '/includes/advanced-custom-fields/acf.php' );
include_once( CHILD_DIR . '/includes/acf-repeater/acf-repeater.php');

//Include Site Structure
include_once( CHILD_DIR . '/includes/structure/_header.php');
include_once( CHILD_DIR . '/includes/structure/_loops.php');
include_once( CHILD_DIR . '/includes/structure/_post.php');
include_once( CHILD_DIR . '/includes/structure/_sidebar.php');
include_once( CHILD_DIR . '/includes/structure/_footer.php');
include_once( CHILD_DIR . '/includes/structure/_searchform.php');

//Include Plugin Overrides
include_once( CHILD_DIR . '/includes/plugins/featured-post-amplified/widget-modifications.php');

//include_once( CHILD_DIR . '/includes/custom-post-types/register-types.php');

// Setup Theme Settings
include_once( CHILD_DIR . '/includes/okimangelo-theme-settings.php');

//Include Shortcodes
include_once( CHILD_DIR . '/includes/shortcodes/shortcodes.php');
include_once( CHILD_DIR . '/includes/shortcodes/post.php');

//Custom Geneis Breadcrumbs
include_once( CHILD_DIR . '/includes/breadcrumbs/functions.php');
include_once( CHILD_DIR . '/includes/breadcrumbs/breadcrumbs.php');


//* Enqueue Lato Google font
add_action( 'wp_enqueue_scripts', 'genesis_sample_google_fonts' );
function genesis_sample_google_fonts() {
	wp_enqueue_style( 'google-font-lato', '//fonts.googleapis.com/css?family=Lato:300,700', array());
}

//* Add HTML5 markup structure
add_theme_support( 'html5' );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for 3-column footer widgets
//add_theme_support( 'genesis-footer-widgets', 3 ); 

add_action( 'wp_enqueue_scripts', 'okimangelo_load_bootstrap' );

function okimangelo_load_bootstrap() {
	wp_register_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/includes/bootstrap/css/bootstrap.css' );
	wp_register_style( 'bootstrap-theme', get_stylesheet_directory_uri() . '/includes/bootstrap/css/bootstrap-theme.css' );

	wp_register_style( 'font-awesome-cdn', 'http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css' );
	wp_register_style( 'flexslider-css', get_stylesheet_directory_uri() . '/includes/plugins/flexslider/flexslider.css' );
	wp_enqueue_style( 'bootstrap-css' );
	wp_enqueue_style( 'bootstrap-theme' );
	wp_enqueue_style( 'font-awesome-cdn' );
	wp_enqueue_style( 'flexslider-css' );

	wp_enqueue_script('jquery');
	wp_register_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/includes/bootstrap/js/bootstrap.js' , array('jquery'));
	wp_register_script( 'flexslider-js', get_stylesheet_directory_uri() . '/includes/plugins/flexslider/jquery.flexslider.js' , array('jquery'));
	wp_register_script( 'custom-js', get_stylesheet_directory_uri() . '/includes/js/custom.js' , array('jquery'));
	wp_register_script( 'cycle-js', get_stylesheet_directory_uri() . '/includes/plugins/cycle2/jquery.cycle2.min.js' , array('jquery'));
	wp_enqueue_script( 'bootstrap-js' );
	wp_enqueue_script( 'flexslider-js' );
	wp_enqueue_script( 'cycle-js' );
	wp_enqueue_script( 'custom-js' );
}

add_action( 'wp_enqueue_scripts', 'okimangelo_load_theme_styles' );

function okimangelo_load_theme_styles() {
    wp_register_style( 'global-styles', get_stylesheet_directory_uri() . '/includes/css/global.css','', time() );
    wp_enqueue_style( 'global-styles' );

    wp_register_style( 'custom-styles', get_stylesheet_directory_uri() . '/includes/css/custom.css' );
    wp_enqueue_style( 'custom-styles' );

    wp_register_style( 'mobile-styles', get_stylesheet_directory_uri() . '/includes/css/mobile.css' );
    wp_enqueue_style( 'mobile-styles' );

}

//Enable Shortcode in Text Widget
add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode');

//* Remove 'You are here' from the front of breadcrumb trail
add_filter( 'genesis_breadcrumb_args', 'rvam_prefix_breadcrumb' );
function rvam_prefix_breadcrumb( $args ) {

    $args['labels']['prefix'] = '';
    return $args;

}

/**
 * Remove Genesis Page Templates
 *
 * @author Bill Erickson
 * @link http://www.billerickson.net/remove-genesis-page-templates
 *
 * @param array $page_templates
 * @return array
 */
function be_remove_genesis_page_templates( $page_templates ) {
    unset( $page_templates['page_archive.php'] );
//    unset( $page_templates['page_blog.php'] );
    return $page_templates;
}
add_filter( 'theme_page_templates', 'be_remove_genesis_page_templates' );

//Remove Form Allowed Tags on Comment Forms
function remove_comment_form_allowed_tags() {
    add_filter('comment_form_defaults','wordpress_comment_form_defaults');
}
add_action('after_setup_theme','remove_comment_form_allowed_tags');
/**
 * @author Brad Dalton - WP Sites
 * @learn more http://wp.me/p1lTu0-9Yd
 */
function wordpress_comment_form_defaults($default) {
    unset($default['comment_notes_after']);
    unset($default['comment_notes_before']);
    return $default;
}


//remove_filter( 'the_content', 'wpautop' );
//remove_filter( 'the_excerpt', 'wpautop' );


//Add Image Size

add_image_size('member-thumb-image', 135,160);
add_image_size('services-thumb-image', 304,224);
add_image_size('blog-featured-image', 475,230);
add_image_size('single-featured-image', 617,230);


