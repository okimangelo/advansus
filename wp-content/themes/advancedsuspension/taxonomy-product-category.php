<?php

//* Force layout to have sidebar
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//* Add custom class to entry
function yvn_entry_content_extraclass( $attributes ) {
  $attributes['class'] = $attributes['class']. ' col-md-4';
    return $attributes;
}
add_filter( 'genesis_attr_entry', 'yvn_entry_content_extraclass' );

//* Replace default loop
remove_action( 'genesis_loop' , 'genesis_do_loop');
add_action( 'genesis_loop' , 'yvn_product_loop');

function yvn_product_loop() {
	global $post;
	
	$output = '';

	$output .= '<div class="container">';
	$output .= '<div class="col-md-12">';
	$image_url = '';
	if (  have_posts(  )  ) : while (  have_posts(  )  ) : the_post(  );
		
		$output .= '<article '.genesis_attr( 'entry' ).'>';
		
		$image = get_the_post_thumbnail( $post->ID  , 'full', array('class'=> 'img-responsive', 'alt'	=> "", 'title'	=> "" ) );
		$image_url = wp_get_attachment_url(  get_post_thumbnail_id(  $post->ID  )  );
		$output_images = '';
		//$output_images .= '<div class="cycle_image">';
		$output_images .= '<div class="cycle-slideshow" data-cycle-fx=scrollHorz data-cycle-timeout=0 data-cycle-pager-template="" data-cycle-pager=".navigation" >';
		$output_images .= '<img class="taxonomy_image" data-image-color="' . get_field( 'default_color' ) . '" src="' . $image_url . '" />';

		//$output .= '<img data-taxonomy-product-color="' . get_field( 'default_color' ) . '" class="img-responsive" alt="placeholder image" src="' . $image_url . '" />';
		$output_navigation = '';
		$output_navigation .= '<span data-single-product-color="' . get_field( 'default_color' ) . '" class="taxonomy_color_chooser" style="background-color:' . get_field( 'default_color' ) . '"></span>';

		if( get_field('add_more_colors') ) {
			while( has_sub_field('additional_galleries') ) {
				$color = get_sub_field('gallery_color');
				$output_navigation .= '<span data-single-product-color="' . $color . '" class="taxonomy_color_chooser" style="background-color:' . $color . '"></span>';
				while( has_sub_field('gallery_image') ) {
					$output_images .= '<img style="display:none;" data-image-color="' . $color . '" class="img-responsive" alt="placeholder image taxonomy_image" src="' . get_sub_field('gallery_item') . '" />';
					break;
				}
			}
		}
		$output .= $output_images;
		$output .= '<header '.genesis_attr( 'entry-header' ).'><h1 class="entry-title" itemprop="headline" >'.get_the_title().'</h1></header>';
		$output .= '</div>';
		$output .= '<div class="navigation">' . $output_navigation . '</div>';		
		//$output .= '<div class="navigation">';
		//$output .= $output_navigation;
		//$output .= '</div>';
		$output .= '</article>';

	endwhile; endif;
	
	$output .= '</div></div>';
	
	echo $output;
}

genesis();