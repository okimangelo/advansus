<?php
//Template Name: Page - Home

remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_loop', 'okimangelo_homepage_loop' );

function okimangelo_homepage_loop() { ?>
    <!-- Start the Loop. -->
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<section class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <article class="entry">
                <?php the_content(); ?>
             </article>
	    </div>
	</section>
<!-- REALLY stop The Loop. -->
        <?php endwhile; else: ?>
            <p>Sorry, no posts matched your criteria.</p>
<!-- REALLY stop The Loop. -->
<?php endif;
    }
genesis();
