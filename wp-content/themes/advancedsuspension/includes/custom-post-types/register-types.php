<?php

/**
 * Create Distributor post type
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

function pdh_register_distributor_post_type() {
    $labels = array(

        'name' => _x( 'Distributors', 'distributors' ),
        'singular_name' => _x( 'Distributor', 'distributors' ),
        'add_new' => _x( 'Add New', 'distributor' ),
        'add_new_item' => _x( 'Add New Distributor', 'distributors' ),
        'edit_item' => _x( 'Edit Distributor', 'distributor' ),
        'new_item' => _x( 'New Distributors', 'distributors' ),
        'view_item' => _x( 'View Distributors', 'distributors' ),
        'search_items' => _x( 'Search Distributors', 'distributors' ),
        'not_found' => _x( 'No Distributors found', 'distributors' ),
        'not_found_in_trash' => _x( 'No Distributors found in Trash', 'distributors' ),
        'parent_item_colon' => _x( 'Parent Distributors:', 'distributors' ),
        'menu_name' => _x( 'Distributors', 'distributors' )
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','thumbnail','editor')
    );

    register_post_type( 'distributors', $args );
}
add_action( 'init', 'pdh_register_distributor_post_type' );

/**
 * Register Distributors taxonomies
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function pdh_register_distributors_taxonomy() {
    $labels = array(
        'name' => _x( 'Distributors Types', 'text-domain'),
        'singular_name' => _x( 'Distributor Type', 'text-domain' ),
        'search_items' => _x( 'Search Distributors Types', 'text-domain' ),
        'popular_items' => _x( 'Popular Distributors Types', 'text-domain' ),
        'all_items' => _x( 'All Distributors Types', 'text-domain' ),
        'parent_item' => _x( 'Parent Distributors Type', 'text-domain' ),
        'parent_item_colon' => _x( 'Parent Distributors Type:', 'text-domain' ),
        'edit_item' => _x( 'Edit Distributors Type', 'text-domain' ),
        'update_item' => _x( 'Update Distributors Type', 'text-domain' ),
        'add_new_item' => _x( 'Add New Distributors Type', 'text-domain' ),
        'new_item_name' => _x( 'New Distributors Type', 'text-domain' ),
        'separate_items_with_commas' => _x( 'Separate Distributors Types with commas', 'text-domain' ),
        'add_or_remove_items' => _x( 'Add or remove Distributors Types', 'text-domain' ),
        'choose_from_most_used' => _x( 'Choose from the most used Distributors Types', 'text-domain' ),
        'menu_name' => _x( 'Distributors Types', 'text-domain' ),
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_admin_column' => true,
        'hierarchical' => true,
        'rewrite' => true,
        'query_var' => 'distributors_types'
    );
    register_taxonomy( 'distributors_types', array('distributors'), $args );
}
add_action( 'init', 'pdh_register_distributors_taxonomy' );