<?php

class Child_Theme_Settings extends Genesis_Admin_Boxes {
	
	/**
	 * Create an admin menu item and settings page.
	 * 
	 * @since 1.0.0
	 */
	function __construct() {
		
		// Specify a unique page ID. 
		$page_id = 'advanced_supension_settings';
		
		// Set it as a child to genesis, and define the menu and page titles
		$menu_ops = array(
			'submenu' => array(
				'parent_slug' => 'genesis',
				'page_title'  => 'Advanced Suspension - Child Theme Settings',
				'menu_title'  => 'Advanced Suspension Theme Settings',
			)
		);
		
		// Set up page options. These are optional, so only uncomment if you want to change the defaults
		$page_ops = array(
		//	'screen_icon'       => 'options-general',
		//	'save_button_text'  => 'Save Settings',
		//	'reset_button_text' => 'Reset Settings',
		//	'save_notice_text'  => 'Settings saved.',
		//	'reset_notice_text' => 'Settings reset.',
		);		
		
		// Give it a unique settings field. 
		// You'll access them from genesis_get_option( 'option_name', 'child-settings' );
		$settings_field = 'okimangelo-settings';
		
		// Set the default values
		$default_settings = array(
            'office_address' => 'addres',
            'mobile_number' => '0212',
            'landline_number' => '',
            'support_email' => '',
			'footer_copy'  => '',
		);
		
		// Create the Admin Page
		$this->create( $page_id, $menu_ops, $page_ops, $settings_field, $default_settings );
 
		// Initialize the Sanitization Filter
		add_action( 'genesis_settings_sanitizer_init', array( $this, 'sanitization_filters' ) );
			
	}
 
	/** 
	 * Set up Sanitization Filters
	 *
	 * See /lib/classes/sanitization.php for all available filters.
	 *
	 * @since 1.0.0
	 */	
	function sanitization_filters() {
        genesis_add_option_filter( 'no-html', $this->settings_field,
            array(
                'address',
                'mobile',
                'landline',
                'support_email',
            ));
		
		genesis_add_option_filter( 'html', $this->settings_field,
			array(
				'footer_copy',
			) );


	}
	

	
	/**
	 * Register metaboxes on Child Theme Settings page
	 *
	 * @since 1.0.0
	 *
	 *
	 */
	function metaboxes() {
        add_meta_box('contact-info', 'Contact Information Section', array( $this, 'okimangelo_contact_info' ), $this->pagehook, 'main', 'high');
		add_meta_box('footer-copyright', 'Footer Copyright Section', array( $this, 'okimangelo_footer_copyright' ), $this->pagehook, 'main', 'high');
	}

    /**
     * Callback for Footer Copyright
     *
     * @since 1.0.0
     *
     */
    function okimangelo_contact_info() { ?>
        <h4>Your Contact information</h4>
        <div>
            <label for="<?php echo $this->get_field_name( 'address', 'okimangelo-settings' ) ;?>" >
                <span>Address</span>
                <input type="text" id="<?php echo $this->get_field_id( 'address', 'okimangelo-settings' ) ; ?>" name="<?php echo $this->get_field_name( 'address', 'okimangelo-settings' ) ; ?>" value="<?php echo genesis_get_option( 'address', 'okimangelo-settings' ); ?> ">
            </label>
        </div>

        <div>
            <label for="<?php echo $this->get_field_name( 'mobile', 'okimangelo-settings' ) ;?>" >
                <span>Mobile</span>
                <input type="text" id="<?php echo $this->get_field_id( 'mobile', 'okimangelo-settings' ) ; ?>" name="<?php echo $this->get_field_name( 'mobile', 'okimangelo-settings' ) ; ?>" value="<?php echo genesis_get_option( 'mobile', 'okimangelo-settings' ); ?> ">
            </label>
        </div>

        <div>
            <label for="<?php echo $this->get_field_name( 'landline', 'okimangelo-settings' ) ;?>" >
                <span>Landline</span>
                <input type="text" id="<?php echo $this->get_field_id( 'landline', 'okimangelo-settings' ) ; ?>" name="<?php echo $this->get_field_name( 'landline', 'okimangelo-settings' ) ; ?>" value="<?php echo genesis_get_option( 'landline', 'okimangelo-settings' ); ?> ">
            </label>
        </div>

        <div>
            <label for="<?php echo $this->get_field_name( 'support_email', 'okimangelo-settings' ) ;?>" >
                <span>Support Email</span>
                <input type="text" id="<?php echo $this->get_field_id( 'support_email', 'okimangelo-settings' ) ; ?>" name="<?php echo $this->get_field_name( 'support_email', 'okimangelo-settings' ) ; ?>" value="<?php echo genesis_get_option( 'support_email', 'okimangelo-settings' ); ?> ">
            </label>
        </div>

    <?php }
	
	/**
	 * Callback for Footer Copyright
	 *
	 * @since 1.0.0
     *
	 */
	function okimangelo_footer_copyright() { ?>
		<div class="general-info">
			<div>
                <label for="<?php echo $this->get_field_name( 'footer_copy', 'okimangelo-settings' ) ;?>">Footer Copyright Section</label>
                <br/>
                <textarea cols="50" rows="10" type="text" id="<?php echo $this->get_field_id( 'footer_copy', 'okimangelo-settings' ) ; ?>" name="<?php echo $this->get_field_name( 'footer_copy', 'okimangelo-settings' ) ; ?>"><?php echo genesis_get_option( 'footer_copy', 'okimangelo-settings' ); ?></textarea>
			</div>
		</div>
		
				
	<?php }

}
 
 
add_action( 'genesis_admin_menu', 'okimangelo_add_child_theme_settings' );
/**
 * Add the Theme Settings Page
 *
 * @since 1.0.0
 */
function okimangelo_add_child_theme_settings() {
	global $_child_theme_settings;
	$_child_theme_settings = new Child_Theme_Settings;	 	
}

function okimangelo_theme_settings_scripts(){
	wp_register_script( 'okimangelo_image_upload', get_stylesheet_directory_uri() .'/includes/js/image-upload.js', array('jquery','media-upload','thickbox') );
	wp_register_style( 'okimangelo_admin_styles', get_stylesheet_directory_uri() .'/includes/css/admin-styles.css');
	wp_enqueue_script('jquery');
	wp_enqueue_script('thickbox');
	wp_enqueue_style('thickbox');
	wp_enqueue_script( 'common' );
	wp_enqueue_script( 'wp-lists' );
	wp_enqueue_script( 'postbox' );
	wp_enqueue_media();
	wp_enqueue_script('okimangelo_image_upload');
	wp_enqueue_style('okimangelo_admin_styles');
}

add_action( 'admin_enqueue_scripts', 'okimangelo_theme_settings_scripts' );
