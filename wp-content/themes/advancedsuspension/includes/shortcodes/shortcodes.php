<?php

/**
 * Sitename shortcode
 */
add_shortcode( 'sitename', 'do_sitename' );
function do_sitename() { echo get_bloginfo(); }


//Contact Information Shortcode
function display_contact_info( $atts ) {
    $content = '<address class="contact-info">';
    $content .= '<p><span class="icon-home">'.genesis_get_option('address','okimangelo-settings').'</span></p>';
    $content .= '<p><a class="icon-mobile" href="tel:'.genesis_get_option('mobile','okimangelo-settings').'">'.genesis_get_option('mobile','okimangelo-settings').'</a></p>';
    $content .= '<p><a class="icon-phone-blue" href="tel:'.genesis_get_option('landline','okimangelo-settings').'">'.genesis_get_option('landline','okimangelo-settings').'</a></p>';
    $content .= '<p><a class="icon-email underlined" href="mailto:'.genesis_get_option('support_email','okimangelo-settings').'?Subject=From%20Contact%20Page: " target="_top"">'.genesis_get_option('support_email','okimangelo-settings').'</a></p>';
    $content .= '<p><a class="icon-web underlined" href="'.get_bloginfo('url').'">'.get_bloginfo('url').'</a></p>';
    $content .= '</<address>';

    return $content;
}
add_shortcode( 'contact_info', 'display_contact_info' );