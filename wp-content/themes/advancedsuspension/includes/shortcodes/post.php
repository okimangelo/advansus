<?php

/* ----------------- POST SHORTCODES ----------------------------*/
/**
 ***
 * This file defines return functions to be used as shortcodes
 * in the post-info and post-meta sections.
 *
 * @package Genesis
 *
 * @example <code>[post_something]</code>
 * @example <code>[post_something before="<em>" after="</em>" foo="bar"]</code>
 */

add_shortcode('mc_post_tags', 'mc_post_tags_shortcode');
/**
 * This function produces the tag link list
 *
 * @since Unknown
 *
 * @example <code>[post_tags]</code> is the default usage
 * @example <code>[post_tags sep=", " before="Tags: " after="bar"]</code>
 *
 * @param array $atts Shortcode attributes
 * @return string
 */
function mc_post_tags_shortcode($atts)
{

    $defaults = array(
        'sep' => ', ',
        'before' => __('Tagged With: ', 'genesis'),
        'after' => ''
    );
    $atts = shortcode_atts($defaults, $atts);

    $tags = get_the_tag_list($atts['before'], trim($atts['sep']) . ' ', $atts['after']);

    if (!$tags) return;

    $output = sprintf('<span class="tags grid_2 omega">%s</span> ', $tags);

    return apply_filters('genesis_post_tags_shortcode', $output, $atts);

}

add_shortcode('mc_post_categories', 'mc_post_categories_shortcode');
/**
 * This function produces the category link list
 *
 * @since Unknown
 *
 * @example <code>[post_categories]</code> is the default usage
 * @example <code>[post_categories sep=", "]</code>
 *
 * @param array $atts Shortcode attributes
 * @return string
 */
function mc_post_categories_shortcode($atts)
{

    $defaults = array(
        'sep'    => '<span>,</span> ',
        'before' => '',
        'after'  => '',
    );

    $atts = shortcode_atts( $defaults, $atts, 'post_categories' );

    $cats = get_the_category_list( trim( $atts['sep'] ) . ' ' );

    if ( genesis_html5() )
        $output = sprintf( '<span %s>', genesis_attr( 'entry-categories' ) ).'<span class="glyphicon glyphicon-pencil"></span>' . $atts['before'] . $cats . $atts['after'] . '</span>';
    else
        $output = '<span class="categories">' . $atts['before'] . $cats . $atts['after'] . '</span>';

    return apply_filters( 'genesis_post_categories_shortcode', $output, $atts );

}

add_shortcode( 'mc_post_date', 'mc_post_date_shortcode' );
/**
 * Produces the date of post publication.
 *
 * Supported shortcode attributes are:
 *   after (output after link, default is empty string),
 *   before (output before link, default is empty string),
 *   format (date format, default is value in date_format option field),
 *   label (text following 'before' output, but before date).
 *
 * Output passes through 'genesis_post_date_shortcode' filter before returning.
 *
 * @since 1.1.0
 *
 * @param array|string $atts Shortcode attributes. Empty string if no attributes.
 * @return string Shortcode output
 */
function mc_post_date_shortcode( $atts ) {

    $defaults = array(
        'after'  => '',
        'before' => '',
        'format' => get_option( 'date_format' ),
        'label'  => '',
    );

    $atts = shortcode_atts( $defaults, $atts, 'post_date' );

//    $display = ( 'relative' === $atts['format'] ) ? genesis_human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) . ' ' . __( 'ago', 'genesis' ) : get_the_time( $atts['format'] );
    $display = '<span class="post-day oswald">'.get_the_time('j').'</span>';
    $display .= '<span class="post-month-year oswald">'.get_the_time('M Y').'</span>';

    if ( genesis_html5() )
        $output = sprintf( '<time %s>', genesis_attr( 'entry-time' ) ) . $atts['before'] . $atts['label'] . $display . $atts['after'] . '</time>';
    else
        $output = sprintf( '<span class="date published time" title="%5$s">%1$s%3$s%4$s%2$s</span> ', $atts['before'], $atts['after'], $atts['label'], $display, get_the_time( 'c' ) );

    return apply_filters( 'genesis_post_date_shortcode', $output, $atts );

}

add_shortcode( 'mc_post_author_posts_link', 'mc_post_author_posts_link_shortcode' );
/**
 * Produces the author of the post (link to author archive).
 *
 * Supported shortcode attributes are:
 *   after (output after link, default is empty string),
 *   before (output before link, default is empty string).
 *
 * Output passes through 'genesis_post_author_posts_link_shortcode' filter before returning.
 *
 * @since 1.1.0
 *
 * @param array|string $atts Shortcode attributes. Empty string if no attributes.
 * @return string Shortcode output
 */
function mc_post_author_posts_link_shortcode( $atts ) {

    $defaults = array(
        'after'  => '',
        'before' => '',
    );

    $atts = shortcode_atts( $defaults, $atts, 'post_author_posts_link' );

    $author = get_the_author();
    $url    = get_author_posts_url( get_the_author_meta( 'ID' ) );

    if ( genesis_html5() ) {
        $output  = sprintf( '<span %s>', genesis_attr( 'entry-author' ) );
        $output .= $atts['before'];
        $output .= '<span class="glyphicon glyphicon-user"></span>';
        $output .= sprintf( '<a href="%s" %s>', $url, genesis_attr( 'entry-author-link' ) );
        $output .= sprintf( '<span %s>', genesis_attr( 'entry-author-name' ) );
        $output .= esc_html( $author );
        $output .= '</span></a>' . $atts['after'] . '</span>';
    } else {
        $link   = sprintf( '<a href="%s" title="%s" rel="author">%s</a>', esc_url( $url ), esc_attr( $author ), esc_html( $author ) );
        $output = sprintf( '<span class="author vcard">%2$s<span class="fn">%1$s</span>%3$s</span>', $link, $atts['before'], $atts['after'] );
    }

    return apply_filters( 'genesis_post_author_posts_link_shortcode', $output, $atts );

}

add_shortcode( 'mc_post_comments', 'mc_post_comments_shortcode' );
/**
 * Produces the link to the current post comments.
 *
 * Supported shortcode attributes are:
 *   after (output after link, default is empty string),
 *   before (output before link, default is empty string),
 *   hide_if_off (hide link if comments are off, default is 'enabled' (true)),
 *   more (text when there is more than 1 comment, use % character as placeholder
 *     for actual number, default is '% Comments')
 *   one (text when there is exactly one comment, default is '1 Comment'),
 *   zero (text when there are no comments, default is 'Leave a Comment').
 *
 * Output passes through 'genesis_post_comments_shortcode' filter before returning.
 *
 * @since 1.1.0
 *
 * @param array|string $atts Shortcode attributes. Empty string if no attributes.
 * @return string Shortcode output
 */
function mc_post_comments_shortcode( $atts ) {

    $defaults = array(
        'after'       => '',
        'before'      => '',
        'hide_if_off' => 'enabled',
        'more'        => __( '% Comments', 'genesis' ),
        'one'         => __( '1 Comment', 'genesis' ),
        'zero'        => __( '0 Comment', 'genesis' ),
    );
    $atts = shortcode_atts( $defaults, $atts, 'post_comments' );

    if ( ( ! genesis_get_option( 'comments_posts' ) || ! comments_open() ) && 'enabled' === $atts['hide_if_off'] )
        return;

    // Darn you, WordPress!
    ob_start();
    comments_number( $atts['zero'], $atts['one'], $atts['more'] );
    $comments = ob_get_clean();

    $comments = sprintf( '<a href="%s">%s</a>', get_comments_link(), $comments );

    $output = genesis_markup( array(
        'html5' => '<span class="entry-comments-link">'.'<span class="glyphicon glyphicon-comment"></span>' . $atts['before'] . $comments . $atts['after'] . '</span>',
        'xhtml' => '<span class="post-comments">' . $atts['before'] . $comments . $atts['after'] . '</span>',
        'echo'  => false,
    ) );

    return apply_filters( 'genesis_post_comments_shortcode', $output, $atts );

}

add_shortcode('mc_post_title','do_mc_post_title');

function do_mc_post_title() {
    return '<h1 class="entry-title" itemprop="headline">'.get_the_title().'</h1>';
}
