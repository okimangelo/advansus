// Script to Upload Image
jQuery( function( $ ){

	$( '.general-info' ).on( 'click', '#upload_image_button', function() {
		//console.log('here');
		var old_send_to_editor = wp.media.editor.send.attachment;
		var input = this;
		wp.media.editor.send.attachment = function( props, attachment ) {
			//props.size = 'medium';
			props = wp.media.string.props( props, attachment );
			props.align = null;
			$(input).parents( 'label' ).find( '#child_settings_logo' ).val( props.src );
			$( '#upload_logo_preview' ).find( '.upload_preview_img' ).attr( 'src', props.src );
			wp.media.editor.send.attachment = old_send_to_editor;
		}
		wp.media.editor.open( input );
	} );

} );