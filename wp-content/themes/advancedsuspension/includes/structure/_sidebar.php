<?php

add_filter( 'genesis_attr_sidebar-primary', 'mc_genesis_attributes_sidebar_primary' );
/**
 * Add attributes for primary sidebar element.
 *
 * @since 2.0.0
 *
 * @param array $attributes Existing attributes.
 *
 * @return array Amended attributes.
 */
function mc_genesis_attributes_sidebar_primary( $attributes ) {

    $attributes['class']     = 'sidebar sidebar-primary widget-area col-md-3';
    $attributes['role']      = 'complementary';
    $attributes['itemscope'] = 'itemscope';
    $attributes['itemtype']  = 'http://schema.org/WPSideBar';

    return $attributes;

}