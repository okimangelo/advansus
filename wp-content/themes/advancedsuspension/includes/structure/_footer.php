<?php

//FOOTER SECTION ---------------------------------------------------------------------------------------


remove_action('genesis_footer', 'genesis_footer_markup_open', 5);
add_action('genesis_footer', 'custom_footer_markup_open', 5);
/**
 * Echos the opening div tag for the footer
 *
 * @since 1.2
 */
function custom_footer_markup_open()
{

    echo '<footer id="footer" class="footer clearfix container">';
    genesis_structural_wrap('footer', 'open');


}


//FOOTER CONTENT -----------------------------------------------
remove_action('genesis_footer', 'genesis_do_footer');
add_action('genesis_footer', 'do_custom_footer');
/**
 * Echo the contents of the footer. Execute any shortcodes that might be present.
 *
 * @since 1.0.1
 */
function do_custom_footer()
{
    echo '<div class="row">';
    echo apply_filters('footer_copy_do_shorcode',genesis_get_option( 'footer_copy', 'okimangelo-settings'));
    echo '</div>';
}

remove_action('genesis_footer', 'genesis_footer_markup_close', 15);
add_action('genesis_footer', 'custom_footer_markup_close', 15);
/**
 * Echos the closing div tag for the footer
 *
 * @since 1.2
 */
function custom_footer_markup_close()
{

    genesis_structural_wrap('footer', 'close');
    echo '</div><!-- end #footer -->' . "\n";

}

