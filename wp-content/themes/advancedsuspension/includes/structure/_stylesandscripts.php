<?php 


/* ----------------------- LOAD SCRIPTS -----------------------------------------
 *	
 */

function mac_header_scripts()
{  
  
    //modernzr.js
    wp_register_script('modernizr', CHILD_URL . '/js/libs/modernizr-2.6.2.min.js','', '2.6.2',false);
    wp_enqueue_script('modernizr');
    
    wp_deregister_script('jquery');
//    wp_register_script('jquery', 'http://code.jquery.com/jquery-1.9.1.min.js','', '1.9.1',false);

    wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js','', '1.10.1',false);
    wp_enqueue_script('jquery');

}

add_action('wp_enqueue_scripts', 'mac_header_scripts');



/* ----------------------- LOAD STYLESHEETS -----------------------------------------
 *	
 */

////Load Genesis Stylesheet
//add_action('wp_print_styles', 'load_genesis_css');
//
//function load_genesis_css()
//{
//    $GENESISURL = PARENT_URL . '/style.css';
//    $GENESISFile = PARENT_DIR . '/style.css';
//
//    if (file_exists($GENESISFile)) {
//        wp_register_style('GenesisDefault', $GENESISURL,'');
//        wp_enqueue_style('GenesisDefault');
//
//    }
//
//
//}



/*Load Theme Stylesheets */


add_action('wp_print_styles', 'load_theme_styles');
function load_theme_styles()
{
  
        wp_register_style('Global-Styles', CHILD_URL .'/css/global.css');
        wp_enqueue_style('Global-Styles');
        wp_register_style('Text-Styles', CHILD_URL .'/css/text.css');
        wp_enqueue_style('Text-Styles');
        wp_register_style('Structure-Styles', CHILD_URL .'/css/custom-structure.css');
        wp_enqueue_style('Structure-Styles');
        wp_register_style('Theming-Styles', CHILD_URL .'/css/custom-theming.css');
        wp_enqueue_style('Theming-Styles',false, '', time(),'all');
    

}



// ------------------------------------ Add Footer scripts ------------------------------------------

add_action('wp_print_footer_scripts', 'footer_scripts');

function footer_scripts()
{

    $content = '<!-- JavaScript at the bottom for fast page loading -->

<!-- <script defer src="' . CHILD_URL . '/bootstrap/js/bootstrap.min.js"></script> -->
<script defer src="' . CHILD_URL . '/js/plugins.js"></script>
<script defer src="' . CHILD_URL . '/js/script.js"></script>';
    if(is_page(array('177','request-an-aircraft-insurance-quote'))){
//             $content .='<script defer src="' . CHILD_URL . '/js/quote.js"></script>';
        $content .='<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>';

    }

    $content .= '<!-- <script src="http://cdn.jquerytools.org/1.2.6/all/jquery.tools.min.js"></script> -->

<!-- end scripts--> 

<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
		chromium.org/developers/how-tos/chrome-frame-getting-started --> 
<!--[if lt IE 7 ]>
		<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
		<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
		<![endif]-->';

    echo $content;
}