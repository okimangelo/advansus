<?php


//HEAD SECTION -------------------------------------------------------------------------------

remove_action('genesis_doctype', 'genesis_do_doctype');
add_action('genesis_doctype', 'custom_do_doctype');

function custom_do_doctype()
{
    ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes('xhtml'); ?>>
<head profile="http://gmpg.org/xfn/11">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta http-equiv="Content-Type"
          content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>
    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">
<?php

}


//HEADER SECTION -------------------------------------------------------------------------





//HEADER CONTENT ----------------------------------------------------


//replace default genesis header with custom header
remove_action('genesis_header', 'genesis_do_header');
//move site title and description to the sidebar
add_filter('genesis_before_sidebar_widget_area', 'genesis_do_custom_header');

function genesis_do_custom_header()
{


    echo '<div id="title-area" class="text-center col-md-12">';
    do_action('genesis_site_title');
    do_action('genesis_site_description');
    echo '</div><!-- end #title-area -->';

//    echo '<div class="row-fluid">';
    do_action('mac_custom_nav');
//    echo '</div><!-- end .row -->';

    if (is_active_sidebar('header-right') || has_action('genesis_header_right')) {
        echo '<div class="widget-area">';
        do_action('genesis_header_right');
        dynamic_sidebar('header-right');
        echo '</div><!-- end .widget-area -->';
    }



    if (is_page_template('page_blog.php')) {
        //echo get_template_part('header', 'top-blog');
    }
    else {
        //echo get_template_part('header', 'top');
        if (is_home()) {
            //echo get_template_part('home', 'header-middle');
        }


    }
}




// Customiz WP Search Form

//function my_search_form( $form ) {
//
//    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
//    <div>
//    <input type="text" value="' . get_search_query() . '" name="s" id="s" />
//    </div>
//    </form>';
//
//    return $form;
//}
//
//add_filter( 'get_search_form', 'my_search_form' );
//


