<?php

/*
 * Functions file for creating the custom Main and Secondary Navigations
 */




remove_action('genesis_after_header', 'genesis_do_nav');
add_action('mac_custom_nav', 'custom_do_nav');

//Add custom classes to genesis Nav
function custom_do_nav() {
    wp_nav_menu(array(
        'menu' => 'Custom Primary Navigation',
        'container' => '',
        'menu_class' => 'pull-right',
        'menu_id' => 'nav'
    ));
}



// Sub Navigation Code


//remove_action('genesis_after_header', 'genesis_do_subnav');


/**
 * This function  is responsible for displaying the "Secondary Navigation" bar.
 *
 * @uses genesis_nav(), genesis_get_option(), wp_nav_menu
 * @since 1.0.1
 *
 */
function custom_add_subnav() {

	if ( genesis_get_option('subnav') ) {

		if ( has_nav_menu( 'secondary' ) ) {

			$args = array(
				'theme_location' => 'secondary',
				'container' => '',
				'menu_class' => genesis_get_option('subnav_superfish') ? 'nav superfish footer-nav grid_14' : 'nav footer-nav grid_14',
				'echo' => 0
			);

			$subnav = wp_nav_menu( $args );

		}

		elseif ( 'nav-menu' != genesis_get_option( 'subnav_type', 'genesis-vestige' ) ) {

			$args = array(
				'theme_location' => 'secondary',
				'menu_class' => genesis_get_option('subnav_superfish', 'genesis-vestige') ? 'nav superfish footer-nav grid_14' : 'nav footer-nav grid_14',
				'show_home' => genesis_get_option('subnav_home', 'genesis-vestige'),
				'type' => genesis_get_option('subnav_type', 'genesis-vestige'),
				'sort_column' => genesis_get_option('subnav_pages_sort', 'genesis-vestige'),
				'orderby' => genesis_get_option('subnav_categories_sort', 'genesis-vestige'),
				'depth' => genesis_get_option('subnav_depth', 'genesis-vestige'),
				'exclude' => genesis_get_option('subnav_exclude', 'genesis-vestige'),
				'include' => genesis_get_option('subnav_include', 'genesis-vestige'),
				'echo' => false
			);

			$subnav = genesis_nav( $args );

		}

                //hide side navigation at homepage

                if ( !is_home() ) {
                    echo apply_filters( 'genesis_do_subnav', $subnav, $args );
                }

	}

}
