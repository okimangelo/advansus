<?php
/**
 * Replacing the default WordPress search form with an HTML5 version
 *
 */
function html5_search_form( $form ) {
    $form = '<form class="search-form col-md-3 pull-right" role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <input type="search" placeholder="'.__("search").'" value="' . get_search_query() . '" name="s" id="s" class="s search-input glyphicon-search"/>
    <input type="submit" id="searchsubmit" value="Search" class="search-submit"/>
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'html5_search_form' );