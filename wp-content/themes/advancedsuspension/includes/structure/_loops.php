<?php
/**
 * Adds loop structures.
 *
 * @package Genesis
 */


/**
 * Hook loops to the genesis_loop output hook so we can get
 * some front-end output. Pretty basic stuff.
 *
 * @since 1.1
 */
function mac_do_loop() {

    if ( is_page_template( 'page_blog.php' ) ) {
        $include = genesis_get_option( 'blog_cat' );
        $exclude = genesis_get_option( 'blog_cat_exclude' ) ? explode( ',', str_replace( ' ', '', genesis_get_option( 'blog_cat_exclude' ) ) ) : '';
        $paged   = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

        //* Easter Egg
        $query_args = wp_parse_args(
            genesis_get_custom_field( 'query_args' ),
            array(
                'cat'              => $include,
                'category__not_in' => $exclude,
                'showposts'        => genesis_get_option( 'blog_cat_num' ),
                'paged'            => $paged,
            )
        );

        mac_custom_loop( $query_args );
    } else {
        mac_standard_loop();
    }

}

/**
 * Standard loop, meant to be executed without modification in most circumstances where content needs to be displayed.
 *
 * It outputs basic wrapping HTML, but uses hooks to do most of its content output like title, content, post information
 * and comments.
 *
 * The action hooks called are:
 *
 *  - `genesis_before_entry`
 *  - `genesis_entry_header`
 *  - `genesis_before_entry_content`
 *  - `genesis_entry_content`
 *  - `genesis_after_entry_content`
 *  - `genesis_entry_footer`
 *  - `genesis_after_endwhile`
 *  - `genesis_loop_else` (only if no posts were found)
 *
 * @since 1.1.0
 *
 * @uses genesis_html5()       Check for HTML5 support.
 * @uses genesis_legacy_loop() XHTML loop.
 * @uses genesis_attr()        Contextual attributes.
 *
 * @return null Return early after legacy loop if not supporting HTML5.
 */
function mac_standard_loop() {

    //* Use old loop hook structure if not supporting HTML5
    if ( ! genesis_html5() ) {
        genesis_legacy_loop();
        return;
    }

    echo '<hr class="clearfix">';

    if ( have_posts() ) : while ( have_posts() ) : the_post();

        do_action( 'genesis_before_entry' );

        printf( '<article %s>', genesis_attr( 'entry' ) );

        do_action( 'genesis_entry_header' );

        do_action( 'genesis_before_entry_content' );
//        printf( '<div %s>', genesis_attr( 'entry-content' ));
        echo '<div class="'. apply_filters('mc_entry_content_class','entry-content') .'">';
        do_action( 'genesis_entry_content' );
        echo '</div>'; //* end .entry-content
        do_action( 'genesis_after_entry_content' );

        do_action( 'genesis_entry_footer' );

        echo '</article>';

        do_action( 'genesis_after_entry' );

    endwhile; //* end of one post
        do_action( 'genesis_after_endwhile' );

    else : //* if no posts exist
        do_action( 'genesis_loop_else' );
    endif; //* end loop

}

/**
 * This is a custom loop function, and is meant to be executed when a
 * custom query is needed. It accepts arguments in query_posts style
 * format to modify the custom WP_Query object.
 *
 * It outputs basic wrapping HTML, but uses hooks to do most of its
 * content output like Title, Content, Post information, and Comments.
 *
 * @since 1.1
 */
function mac_custom_loop( $args = array() ) {
    global $wp_query, $more;

    $defaults = array(); //* For forward compatibility
    $args     = apply_filters( 'genesis_custom_loop_args', wp_parse_args( $args, $defaults ), $args, $defaults );

    $wp_query = new WP_Query( $args );

    //* Only set $more to 0 if we're on an archive
    $more = is_singular() ? $more : 0;

    mac_standard_loop();

    //* Restore original query
    wp_reset_query();

}

/**
 * Yet another custom loop function.
 * Outputs markup compatible with a Feature + Grid style layout.
 * All normal loop hooks present, except for genesis_post_content.
 *
 * @since 1.5
 */
function mac_grid_loop( $args = array() ) {

    //* Global vars
    global $_genesis_loop_args;

    //* Parse args
    $args = apply_filters(
        'genesis_grid_loop_args',
        wp_parse_args(
            $args,
            array(
                'features'				=> 2,
                'features_on_all'		=> false,
                'feature_image_size'	=> 0,
                'feature_image_class'	=> 'alignleft',
                'feature_content_limit'	=> 0,
                'grid_image_size'		=> 'thumbnail',
                'grid_image_class'		=> 'alignleft',
                'grid_content_limit'	=> 0,
                'more'					=> __( 'Read more', 'genesis' ) . '&#x02026;',
            )
        )
    );

    //* If user chose more features than posts per page, adjust features
    if ( get_option( 'posts_per_page' ) < $args['features'] ) {
        $args['features'] = get_option( 'posts_per_page' );
    }

    //* What page are we on?
    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

    //* Potentially remove features on page 2+
    if ( ! $args['features_on_all'] && $paged > 1 )
        $args['features'] = 0;

    //* Set global loop args
    $_genesis_loop_args = $args;

    //* Remove some unnecessary stuff from the grid loop
    remove_action( 'genesis_before_post_title', 'genesis_do_post_format_image' );
    remove_action( 'genesis_post_content', 'genesis_do_post_image' );
    remove_action( 'genesis_post_content', 'genesis_do_post_content' );
    remove_action( 'genesis_post_content', 'genesis_do_post_content_nav' );

    remove_action( 'genesis_entry_header', 'genesis_do_post_format_image', 4 );
    remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
    remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
    remove_action( 'genesis_entry_content', 'genesis_do_post_content_nav', 12 );
    remove_action( 'genesis_entry_content', 'genesis_do_post_permalink', 14 );


    //* Custom loop output
    add_filter( 'post_class', 'genesis_grid_loop_post_class' );
    add_action( 'genesis_post_content', 'genesis_grid_loop_content' );
    add_action( 'genesis_entry_content', 'genesis_grid_loop_content' );

    //* The loop
    genesis_standard_loop();

    //* Reset loops
    genesis_reset_loops();
    remove_filter( 'post_class', 'genesis_grid_loop_post_class' );
    remove_action( 'genesis_post_content', 'genesis_grid_loop_content' );
    remove_action( 'genesis_entry_content', 'genesis_grid_loop_content' );
}

/**
 * This function filter the post class array to output custom classes
 * for the feature/grid layout, based on the grid loop args and the loop counter.
 *
 * @since 1.5
 */
function mac_grid_loop_post_class( $classes ) {

	global $_genesis_loop_args, $loop_counter;

	$grid_classes = array();

	if ( $_genesis_loop_args['features'] && $loop_counter < $_genesis_loop_args['features'] ) {
		$grid_classes[] = 'genesis-feature';
		$grid_classes[] = sprintf( 'genesis-feature-%s', $loop_counter + 1 );
		$grid_classes[] = $loop_counter&1 ? 'genesis-feature-even' : 'genesis-feature-odd';
	}
	elseif ( $_genesis_loop_args['features']&1 ) {
		$grid_classes[] = 'genesis-grid';
		$grid_classes[] = sprintf( 'genesis-grid-%s', $loop_counter - $_genesis_loop_args['features'] + 1 );
		$grid_classes[] = $loop_counter&1 ? 'genesis-grid-odd' : 'genesis-grid-even';
	}
	else {
		$grid_classes[] = 'genesis-grid';
		$grid_classes[] = sprintf( 'genesis-grid-%s', $loop_counter - $_genesis_loop_args['features'] + 1 );
		$grid_classes[] = $loop_counter&1 ? 'genesis-grid-even' : 'genesis-grid-odd';
	}

	return array_merge( $classes, apply_filters( 'mac_grid_loop_post_class', $grid_classes ) );

}

/**
 * This function outputs specially formatted content, based on the grid loop args.
 *
 * @since 1.5
 */
function mac_grid_loop_content() {

    global $_genesis_loop_args;

    if ( in_array( 'genesis-feature', get_post_class() ) ) {

        if ( $_genesis_loop_args['feature_image_size'] ) {

            $image = genesis_get_image( array(
                'size'    => $_genesis_loop_args['feature_image_size'],
                'context' => 'grid-loop-featured',
                'attr'    => genesis_parse_attr( 'entry-image-grid-loop', array( 'class' => $_genesis_loop_args['feature_image_class'] ) ),
            ) );

            printf( '<a href="%s" title="%s">%s</a>', get_permalink(), the_title_attribute( 'echo=0' ), $image );

        }

        if ( $_genesis_loop_args['feature_content_limit'] )
            the_content_limit( (int) $_genesis_loop_args['feature_content_limit'], esc_html( $_genesis_loop_args['more'] ) );
        else
            the_content( esc_html( $_genesis_loop_args['more'] ) );

    }

    else {

        if ( $_genesis_loop_args['grid_image_size'] ) {

            $image = genesis_get_image( array(
                'size'    => $_genesis_loop_args['grid_image_size'],
                'context' => 'grid-loop',
                'attr'    => genesis_parse_attr( 'entry-image-grid-loop', array( 'class' => $_genesis_loop_args['grid_image_class'] ) ),
            ) );

            printf( '<a href="%s" title="%s">%s</a>', get_permalink(), the_title_attribute( 'echo=0' ), $image );

        }

        if ( $_genesis_loop_args['grid_content_limit'] ) {
            the_content_limit( (int) $_genesis_loop_args['grid_content_limit'], esc_html( $_genesis_loop_args['more'] ) );
        } else {
            the_excerpt();
            printf( '<a href="%s" class="more-link">%s</a>', get_permalink(), esc_html( $_genesis_loop_args['more'] ) );
        }

    }

}