<?php
/**
 * Controls output elements in post structures.
 *
 * @package Genesis
 */

/**
Custom Blog Post Info Structure 
 */

//remove_action( 'genesis_before_post_title', 'genesis_do_post_format_image' );
//add_action( 'genesis_before_post_title', 'mac_do_post_format_image' );
/**
 * Post format icon.
 * Adds an image, corresponding to the post format, before the post title
 *
 * @since 1.4
 */
function mac_do_post_format_image() {

	global $post;

	/** Do nothing if post formats aren't supported */
	if ( ! current_theme_supports( 'post-formats' ) || ! current_theme_supports( 'genesis-post-format-images' ) )
		return;

	/** Get post format */
	$post_format = get_post_format( $post );

	/** If post format is set, look for post format image */
	if ( $post_format && file_exists( sprintf( '%s/images/post-formats/%s.png', CHILD_DIR, $post_format ) ) ) {
		printf( '<a href="%s" title="%s" rel="bookmark"><img src="%s" class="post-format-image" alt="%s" /></a>', get_permalink(), the_title_attribute('echo=0'), sprintf( '%s/images/post-formats/%s.png', CHILD_URL, $post_format ), $post_format );
	}
	/** Else, look for the default post format image */
	elseif ( file_exists( sprintf( '%s/images/post-formats/default.png', CHILD_DIR ) ) ) {
		printf( '<a href="%s" title="%s" rel="bookmark"><img src="%s/images/post-formats/default.png" class="post-format-image" alt="%s" /></a>', get_permalink(), the_title_attribute('echo=0'), CHILD_URL, 'post' );
	}

}

//TODO: Place post title code here
//remove_action('genesis_post_content', 'genesis_do_post_image');
//add_action('genesis_post_content', 'mac_do_post_image');
/**
 * Post Image
 */
function mac_do_post_image() {


    global $post;

    if ( ! is_singular() && genesis_get_option( 'content_archive_thumbnail' ) && has_post_thumbnail() ) {
        $img = genesis_get_image( array( 'format' => 'html', 'size' => genesis_get_option( 'image_size' ), 'attr' => array( 'class' => 'alignleft post-image' ) ) );
        printf( '<a href="%s" title="%s">%s</a>', get_permalink(), the_title_attribute( 'echo=0' ), $img );
    }
	else if ( is_singular() && genesis_get_option('content_archive_thumbnail') ) {
		$img = genesis_get_image( array( 'format' => 'html','size' => 'Slideshow','attr' => array( 'class' => 'alignleft post-image' ) ) );
		printf( '<a href="%s" title="%s">%s</a>', get_permalink(), the_title_attribute('echo=0'), $img );
	}

    else {
        return;
    }




}



remove_action('genesis_post_content', 'genesis_do_post_content');
add_action('genesis_post_content', 'mac_do_post_content');
/**
 * Post Content
 */
function mac_do_post_content() {

	if ( is_singular() ) {
		the_content(); // display content on posts/pages

		if ( is_single() && get_option('default_ping_status') == 'open' ) {
			echo '<!--'; trackback_rdf(); echo '-->' ."\n";
		}

		if ( is_page() ) {
			edit_post_link(__('(Edit)', 'genesis'), '', '');
		}
	}
	elseif ( genesis_get_option('content_archive') == 'excerpts' ) {
		the_excerpt();
	}
	else {
		if ( genesis_get_option('content_archive_limit') )
			the_content_limit( (int)genesis_get_option('content_archive_limit'), __('[Read more...]', 'genesis') );
		else
			the_content(__('[Read more...]', 'genesis'));
	}

	wp_link_pages( array( 'before' => '<p class="pages">' . __( 'Pages:', 'genesis' ), 'after' => '</p>' ) );

}

//remove_action('genesis_loop_else', 'genesis_do_noposts');
//add_action('genesis_loop_else', 'mac_do_noposts');
/**
 * No Posts
 */
function mac_do_noposts() {

	printf( '<p>%s</p>', apply_filters( 'genesis_noposts_text', __('Sorry, no posts matched your criteria.', 'genesis') ) );

}


//add_filter( 'genesis_post_info', 'do_shortcode', 20 );
//add_action( 'genesis_entry_header', 'genesis_post_info', 12 );
//remove_action( 'genesis_before_post_content', 'genesis_post_info' );
//add_action( 'genesis_before_post_content', 'mac_post_info' );
/**
 * Echo the post info (byline) under the post title.
 *
 * Doesn't do post info on pages.
 *
 * The post info makes use of several shortcodes by default, and the whole output is filtered via `genesis_post_info`
 * before echoing.
 *
 * @since 0.2.3
 *
 * @uses genesis_markup() Contextual markup.
 *
 * @global WP_Post $post Post object.
 *
 * @return null Return early if on a page.
 */
function mac_post_info() {

    global $post;

    if ( 'page' === get_post_type( $post->ID ) )
        return;

    if ( is_single()) {
        $post_info = apply_filters( 'genesis_post_info', '<div class="col-md-2 pull-left no-padding">[mc_post_date]</div><div class="col-md-10 pull-left no-padding">[mc_post_title] <section class="post-info-container col-md-12 pull-left no-padding">[mc_post_author_posts_link] [mc_post_categories] [mc_post_comments]</section></div>' );
        genesis_markup( array(
            'html5' => sprintf( '<aside class="entry-meta">%s</aside>', $post_info ),
            'xhtml' => sprintf( '<div class="post-info">%s</div>', $post_info ),
        ) );
    } else {
        $post_info = apply_filters( 'genesis_post_info', '[mc_post_date] [mc_post_author_posts_link] [mc_post_categories] [mc_post_comments]' );
        genesis_markup( array(
            'html5' => sprintf( '<p class="entry-meta col-md-2 pull-right">%s</p>', $post_info ),
            'xhtml' => sprintf( '<div class="post-info">%s</div>', $post_info ),
        ) );

    }

}

//remove_action('genesis_after_post', 'genesis_do_author_box_single');
//add_action('genesis_after_post', 'mac_do_author_box_single');
/**
 * This function runs some conditional code and calls the
 * genesis_author_box() function, if necessary.
 *
 * @uses genesis_author_box()
 *
 * @since 1.0
 */
function mac_do_author_box_single() {

	if ( !is_single() )
		return;

	if ( get_the_author_meta( 'genesis_author_box_single', get_the_author_meta('ID') ) ) {
		mac_author_box( 'single' );
	}

}

/**
 * This function outputs the content of the author box.
 * The title is filterable, and the description is dynamic.
 *
 * @uses get_the_author_meta(), get_the_author
 *
 * @since 1.3
 */
function mac_author_box( $context = '' ) {

	global $authordata;
	$authordata = is_object( $authordata ) ? $authordata : get_userdata( get_query_var('author') );

	$gravatar_size = apply_filters('genesis_author_box_gravatar_size', '70', $context);
	$gravatar = get_avatar( get_the_author_meta('email'), $gravatar_size );
	$title = apply_filters( 'genesis_author_box_title', sprintf( '<strong>%s %s</strong>', __('About', 'genesis'), get_the_author() ), $context );
	$description = wpautop( get_the_author_meta('description') );

	// The author box markup, contextual.
	$pattern = $context == 'single' ? '<div class="author-box"><div>%s %s<br />%s</div></div><!-- end .authorbox-->' : '<div class="author-box">%s<h1>%s</h1><div>%s</div></div><!-- end .authorbox-->';

	echo apply_filters( 'genesis_author_box', sprintf( $pattern, $gravatar, $title, $description ), $context, $pattern, $gravatar, $title, $description );

}

//remove_action('genesis_after_endwhile', 'genesis_posts_nav');
//add_action('genesis_after_endwhile', 'mac_posts_nav');
/**
 * The default post navigation, hooked to genesis_after_endwhile
 *
 * @since 0.2.3
 */
function mac_posts_nav() {
	$nav = genesis_get_option('posts_nav');

	if($nav == 'prev-next')
		mac_prev_next_posts_nav();
	elseif($nav == 'numeric')
		mac_numeric_posts_nav();
	else
		mac_older_newer_posts_nav();
}

/**
 * Display older/newer posts navigation
 *
 * @since 0.2.2
 */
function mac_older_newer_posts_nav() {

	$older_link = get_next_posts_link( apply_filters( 'genesis_older_link_text', g_ent('&laquo; ') . __( 'Older Posts', 'genesis' ) ) );
	$newer_link = get_previous_posts_link( apply_filters( 'genesis_newer_link_text', __('Newer Posts', 'genesis') . g_ent(' &raquo;') ) );

	$older = $older_link ? '<div class="alignleft">' . $older_link . '</div>' : '';
	$newer = $newer_link ? '<div class="alignright">' . $newer_link . '</div>' : '';

	$nav = '<div class="navigation">' . $older . $newer . '</div><!-- end .navigation -->';

	if ( ! empty( $older ) || ! empty( $newer ) )
		echo $nav;
}

/**
 * Display prev/next posts navigation
 *
 * @since 0.2.2
 */
function mac_prev_next_posts_nav() {

	$prev_link = get_previous_posts_link( apply_filters( 'genesis_prev_link_text', g_ent( '&laquo; ' ) . __( 'Previous Page', 'genesis' ) ) );
	$next_link = get_next_posts_link( apply_filters( 'genesis_next_link_text', __( 'Next Page', 'genesis' ) . g_ent( ' &raquo;' ) ) );

	$prev = $prev_link ? '<div class="alignleft">' . $prev_link . '</div>' : '';
	$next = $next_link ? '<div class="alignright">' . $next_link . '</div>' : '';

	$nav = '<div class="navigation">' . $prev . $next . '</div><!-- end .navigation -->';

	if ( !empty( $prev ) || !empty( $next ) )
		echo $nav;
}

/**
 * Display links to previous/next post
 *
 * @since 1.5.1
 */
function mac_prev_next_post_nav() {

	if ( !is_singular('post') )
		return;
?>

	<div class="navigation">
		<div class="alignleft"><?php previous_post_link(); ?></div>
		<div class="alignright"><?php next_post_link(); ?></div>
	</div>

<?php
}

/**
 * Display numeric posts navigation (similar to WP-PageNavi)
 *
 * @since 0.2.3
 */
function mac_numeric_posts_nav() {
	if( is_singular() ) return; // do nothing

	global $wp_query;

	// Stop execution if there's only 1 page
	if( $wp_query->max_num_pages <= 1 ) return;

	$paged = get_query_var('paged') ? absint( get_query_var('paged') ) : 1;
	$max = intval( $wp_query->max_num_pages );

	echo '<div class="navigation"><ul>' . "\n";

	//	add current page to the array
	if ( $paged >= 1 )
		$links[] = $paged;

	//	add the pages around the current page to the array
	if ( $paged >= 3 ) {
		$links[] = $paged - 1; $links[] = $paged - 2;
	}
	if ( ($paged + 2) <= $max ) {
		$links[] = $paged + 2; $links[] = $paged + 1;
	}

	//	Previous Post Link
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link( g_ent( __('&laquo; Previous', 'genesis') ) ) );

	//	Link to first Page, plus ellipeses, if necessary
	if ( !in_array( 1, $links ) ) {
		if ( $paged == 1 ) $current = ' class="active"'; else $current = null;
		printf( '<li %s><a href="%s">%s</a></li>' . "\n", $current, get_pagenum_link(1), '1' );

		if ( !in_array( 2, $links ) )
			echo g_ent('<li>&hellip;</li>');
	}

	//	Link to Current page, plus 2 pages in either direction (if necessary).
	sort( $links );
	foreach( (array)$links as $link ) {
		$current = ( $paged == $link ) ? 'class="active"' : '';
		printf( '<li %s><a href="%s">%s</a></li>' . "\n", $current, get_pagenum_link($link), $link );
	}

	//	Link to last Page, plus ellipses, if necessary
	if ( !in_array( $max, $links ) ) {
		if ( !in_array( $max - 1, $links ) )
			echo g_ent('<li>&hellip;</li>') . "\n";

		$current = ( $paged == $max ) ? 'class="active"' : '';
		printf( '<li %s><a href="%s">%s</a></li>' . "\n", $current, get_pagenum_link($max), $max );
	}

	//	Next Post Link
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link( g_ent( __('Next &raquo;', 'genesis') ) ) );

	echo '</ul></div>' . "\n";
}


// Read More.


add_filter('excerpt_more', 'child_read_more_link');
//add_filter( 'get_the_content_more_link', 'child_read_more_link' );
//add_filter( 'the_content_more_link', 'child_read_more_link' );
function child_read_more_link($output)
{

        return '<br/><div class="grid_2 alpha read-more button medium-button black"><a class="more-link" href="' . get_permalink() . '" rel="nofollow">READ MORE</a></div>
                <div class="addthis-container push_3 grid_2 omega"><a class="addthis_counter addthis_pill_style" addthis:url="'. get_permalink() . '"><a class="atc_s addthis_button_compact"><span></span></a></div>';
}

