<?php
/**
 * Controls output elements in post structures.
 *
 * @package Genesis
 */

/**
Custom Blog Post Info Structure 
 */

add_filter('mac_post_info', 'do_shortcode', 20);
//remove_action('genesis_before_post_content', 'genesis_post_info');
//add_action('genesis_before_post_content', 'mac_post_author');
/**
 * Add the post info (byline) under the title
 *
 * @since 0.2.3
 */
function mac_post_author() {
	
	global $post;

	if ( is_page( $post->ID ) )
		return; // don't do post-info on pages

	$post_info = __('Posted by:', 'genesis') . ' [post_author_posts_link]';
	printf( '<div id="post-author" class="post-info">%s</div>', apply_filters('mac_post_info', $post_info) );

}

add_filter('genesis_post_meta', 'do_shortcode', 20);
//remove_action('genesis_after_post_content', 'genesis_post_meta');
//add_action('genesis_after_post_content', 'mac_post_meta');
/**
 * Add the post meta after the post content
 *
 * @since 0.2.3
 */
function mac_post_meta() {
	
	global $post;

	if ( is_page( $post->ID ) )
		return; // don't do post-meta on pages

	$post_meta = '[mac_post_categories sep=", " before="Categories: "] [mac_post_tags sep=", " before="Tags: "]';
	printf( '<div class="post-meta clearfix">%s</div>', apply_filters('genesis_post_meta', $post_meta) );

}
//remove_action('genesis_after_post', 'genesis_do_author_box_single');
//add_action('genesis_after_post', 'mac_do_author_box_single');
/**
 * This function runs some conditional code and calls the
 * genesis_author_box() function, if necessary.
 *
 * @uses genesis_author_box()
 *
 * @since 1.0
 */
function mac_do_author_box_single() {

	if ( !is_single() )
		return;

	if ( get_the_author_meta( 'genesis_author_box_single', get_the_author_meta('ID') ) ) {
		mac_author_box( 'single' );
	}

}

/**
 * This function outputs the content of the author box.
 * The title is filterable, and the description is dynamic.
 *
 * @uses get_the_author_meta(), get_the_author
 *
 * @since 1.3
 */
function mac_author_box( $context = '' ) {

	global $authordata;
	$authordata = is_object( $authordata ) ? $authordata : get_userdata( get_query_var('author') );

	$gravatar_size = apply_filters('genesis_author_box_gravatar_size', '70', $context);
	$gravatar = get_avatar( get_the_author_meta('email'), $gravatar_size );
	$title = apply_filters( 'genesis_author_box_title', sprintf( '<strong>%s %s</strong>', __('About', 'genesis'), get_the_author() ), $context );
	$description = wpautop( get_the_author_meta('description') );

	// The author box markup, contextual.
	$pattern = $context == 'single' ? '<div class="author-box"><div>%s %s<br />%s</div></div><!-- end .authorbox-->' : '<div class="author-box">%s<h1>%s</h1><div>%s</div></div><!-- end .authorbox-->';

	echo apply_filters( 'genesis_author_box', sprintf( $pattern, $gravatar, $title, $description ), $context, $pattern, $gravatar, $title, $description );

}

//remove_action('genesis_after_endwhile', 'genesis_posts_nav');
//add_action('genesis_after_endwhile', 'mac_posts_nav');
/**
 * The default post navigation, hooked to genesis_after_endwhile
 *
 * @since 0.2.3
 */
function mac_posts_nav() {
	$nav = genesis_get_option('posts_nav');

	if($nav == 'prev-next')
		mac_prev_next_posts_nav();
	elseif($nav == 'numeric')
		mac_numeric_posts_nav();
	else
		mac_older_newer_posts_nav();
}

/**
 * Display older/newer posts navigation
 *
 * @since 0.2.2
 */
function mac_older_newer_posts_nav() {

	$older_link = get_next_posts_link( apply_filters( 'genesis_older_link_text', g_ent('&laquo; ') . __( 'Older Posts', 'genesis' ) ) );
	$newer_link = get_previous_posts_link( apply_filters( 'genesis_newer_link_text', __('Newer Posts', 'genesis') . g_ent(' &raquo;') ) );

	$older = $older_link ? '<div class="alignleft">' . $older_link . '</div>' : '';
	$newer = $newer_link ? '<div class="alignright">' . $newer_link . '</div>' : '';

	$nav = '<div class="navigation">' . $older . $newer . '</div><!-- end .navigation -->';

	if ( ! empty( $older ) || ! empty( $newer ) )
		echo $nav;
}

/**
 * Display prev/next posts navigation
 *
 * @since 0.2.2
 */
function mac_prev_next_posts_nav() {

	$prev_link = get_previous_posts_link( apply_filters( 'genesis_prev_link_text', g_ent( '&laquo; ' ) . __( 'Previous Page', 'genesis' ) ) );
	$next_link = get_next_posts_link( apply_filters( 'genesis_next_link_text', __( 'Next Page', 'genesis' ) . g_ent( ' &raquo;' ) ) );

	$prev = $prev_link ? '<div class="alignleft">' . $prev_link . '</div>' : '';
	$next = $next_link ? '<div class="alignright">' . $next_link . '</div>' : '';

	$nav = '<div class="navigation">' . $prev . $next . '</div><!-- end .navigation -->';

	if ( !empty( $prev ) || !empty( $next ) )
		echo $nav;
}

/**
 * Display links to previous/next post
 *
 * @since 1.5.1
 */
function mac_prev_next_post_nav() {

	if ( !is_singular('post') )
		return;
?>

	<div class="navigation">
		<div class="alignleft"><?php previous_post_link(); ?></div>
		<div class="alignright"><?php next_post_link(); ?></div>
	</div>

<?php
}

/**
 * Display numeric posts navigation (similar to WP-PageNavi)
 *
 * @since 0.2.3
 */
function mac_numeric_posts_nav() {
	if( is_singular() ) return; // do nothing

	global $wp_query;

	// Stop execution if there's only 1 page
	if( $wp_query->max_num_pages <= 1 ) return;

	$paged = get_query_var('paged') ? absint( get_query_var('paged') ) : 1;
	$max = intval( $wp_query->max_num_pages );

	echo '<div class="navigation"><ul>' . "\n";

	//	add current page to the array
	if ( $paged >= 1 )
		$links[] = $paged;

	//	add the pages around the current page to the array
	if ( $paged >= 3 ) {
		$links[] = $paged - 1; $links[] = $paged - 2;
	}
	if ( ($paged + 2) <= $max ) {
		$links[] = $paged + 2; $links[] = $paged + 1;
	}

	//	Previous Post Link
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link( g_ent( __('&laquo; Previous', 'genesis') ) ) );

	//	Link to first Page, plus ellipeses, if necessary
	if ( !in_array( 1, $links ) ) {
		if ( $paged == 1 ) $current = ' class="active"'; else $current = null;
		printf( '<li %s><a href="%s">%s</a></li>' . "\n", $current, get_pagenum_link(1), '1' );

		if ( !in_array( 2, $links ) )
			echo g_ent('<li>&hellip;</li>');
	}

	//	Link to Current page, plus 2 pages in either direction (if necessary).
	sort( $links );
	foreach( (array)$links as $link ) {
		$current = ( $paged == $link ) ? 'class="active"' : '';
		printf( '<li %s><a href="%s">%s</a></li>' . "\n", $current, get_pagenum_link($link), $link );
	}

	//	Link to last Page, plus ellipses, if necessary
	if ( !in_array( $max, $links ) ) {
		if ( !in_array( $max - 1, $links ) )
			echo g_ent('<li>&hellip;</li>') . "\n";

		$current = ( $paged == $max ) ? 'class="active"' : '';
		printf( '<li %s><a href="%s">%s</a></li>' . "\n", $current, get_pagenum_link($max), $max );
	}

	//	Next Post Link
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link( g_ent( __('Next &raquo;', 'genesis') ) ) );

	echo '</ul></div>' . "\n";
}


/* ----------------- POST SHORTCODES ----------------------------*/
/**
 ***
 * This file defines return functions to be used as shortcodes
 * in the post-info and post-meta sections.
 *
 * @package Genesis
 *
 * @example <code>[post_something]</code>
 * @example <code>[post_something before="<em>" after="</em>" foo="bar"]</code>
 */


//add_filter('genesis_post_date_shortcode', 'custom_post_date_shortcode', 10, 2);

/**
 * Customize Post Date format and add extra markup for CSS targeting
 *
 * @author Gary Jones
 * @link http://dev.studiopress.com/style-post-info.htm
 *
 * @param string $output HTML markup
 * @param array $atts Shortcode attributes
 * @return string HTML markup
 */
function custom_post_date_shortcode($output, $atts)
{
    return sprintf('<div class="date time published ribbon-black med-rounded" title="%4$s">%1$s<div class="day">%2$s</div> <div class="month">%3$s</div></div>',
                   $atts['label'],
                   get_the_time('j'),
                   get_the_time('M'),
                   get_the_time('Y-m-d\TH:i:sO')
    );
}


add_shortcode('mac_post_comments', 'mac_post_comments_shortcode');
/**
 * This function produces the comment link
 *
 * @since Unknown
 *
 * @example <code>[post_comments]</code> is the default usage
 * @example <code>[post_comments zero="No Comments" one="1 Comment" more="% Comments"]</code>
 *
 * @param array $atts Shortcode attributes
 * @return string
 */
function mac_post_comments_shortcode($atts)
{

    $defaults = array(
        'zero' => __('0', 'genesis'),
        'one' => __('1', 'genesis'),
        'more' => __('%', 'genesis'),
        'hide_if_off' => 'enabled',
        'before' => '',
        'after' => ''
    );
    $atts = shortcode_atts($defaults, $atts);

    if ((!genesis_get_option('comments_posts') || !comments_open()) && 'enabled' === $atts['hide_if_off'])
        return;

    // Darn you, WordPress!
    ob_start();
    comments_number($atts['zero'], $atts['one'], $atts['more']);
    $comments = ob_get_clean();

    $comments = sprintf('<a href="%s">%s</a>', get_comments_link(), $comments);

    $output = sprintf('<div class="post-comments ribbon-black med-rounded">%2$s%1$s%3$s</div>', $comments, $atts['before'], $atts['after']);

    return apply_filters('genesis_post_comments_shortcode', $output, $atts);

}


add_shortcode('mac_post_tags', 'mac_post_tags_shortcode');
/**
 * This function produces the tag link list
 *
 * @since Unknown
 *
 * @example <code>[post_tags]</code> is the default usage
 * @example <code>[post_tags sep=", " before="Tags: " after="bar"]</code>
 *
 * @param array $atts Shortcode attributes
 * @return string
 */
function mac_post_tags_shortcode($atts)
{

    $defaults = array(
        'sep' => ', ',
        'before' => __('Tagged With: ', 'genesis'),
        'after' => ''
    );
    $atts = shortcode_atts($defaults, $atts);

    $tags = get_the_tag_list($atts['before'], trim($atts['sep']) . ' ', $atts['after']);

    if (!$tags) return;

    $output = sprintf('<span class="tags grid_2 omega">%s</span> ', $tags);

    return apply_filters('genesis_post_tags_shortcode', $output, $atts);

}

add_shortcode('mac_post_categories', 'mac_post_categories_shortcode');
/**
 * This function produces the category link list
 *
 * @since Unknown
 *
 * @example <code>[post_categories]</code> is the default usage
 * @example <code>[post_categories sep=", "]</code>
 *
 * @param array $atts Shortcode attributes
 * @return string
 */
function mac_post_categories_shortcode($atts)
{

    $defaults = array(
        'sep' => ', ',
        'before' => __('Filed Under: ', 'genesis'),
        'after' => ''
    );
    $atts = shortcode_atts($defaults, $atts);

    $cats = get_the_category_list(trim($atts['sep']) . ' ');

    $output = sprintf('<span class="categories grid_4 alpha">%2$s%1$s%3$s</span> ', $cats, $atts['before'], $atts['after']);

    return apply_filters('genesis_post_categories_shortcode', $output, $atts);

}


