<?php

remove_action( 'gfwa_before_post_content', 'gfwa_do_post_title', 10, 1 );

add_action( 'gfwa_before_post_content', 'mec_do_post_title', 10, 1 );

/**
 * Outputs Post Title if option is selects
 *
 * @author Nick Croft
 * @since 0.1
 * @version 0.2
 * @param array $instance Values set in widget isntance
 */
function mec_do_post_title( $instance ) {
    echo '<aside class="custom-featured-post latest-post-entry clearfix">';
    if ( !empty( $instance['show_byline'] ) && !empty( $instance['post_info'] ) )
        printf( '<div class="pull-left post-date"><p class="byline post-info">%s</p></div>', do_shortcode( esc_html( $instance['post_info'] ) ) );
    echo '<div class="pull-right post-title-content">';
    $link = $instance['link_title_field'] && genesis_get_custom_field( $instance['link_title_field']) ? genesis_get_custom_field( $instance['link_title_field']) : get_permalink();

    $wrap_open = $instance['link_title'] == 1 ? sprintf( '<a href="%s" title="%s">', $link, the_title_attribute( 'echo=0' ) ) : '';
    $wrap_close = $instance['link_title'] == 1 ? '</a>' : '';

    if ( !empty( $instance['show_title'] ) && !empty( $instance['title_limit'] ) )
        printf( '<h2>%s%s%s%s</h2>', $wrap_open, genesis_truncate_phrase( the_title_attribute( 'echo=0' ) , $instance['title_limit'] ), $instance['title_cutoff'], $wrap_close );
    elseif ( !empty( $instance['show_title'] ) )
        printf( '<h2>%s%s%s</h2>', $wrap_open, the_title_attribute( 'echo=0' ), $wrap_close );
}




remove_action( 'gfwa_before_post_content', 'gfwa_do_byline');


remove_action( 'gfwa_post_content', 'gfwa_do_post_content', 10, 1 );
add_action( 'gfwa_post_content', 'mec_do_post_content', 10, 1 );

/**
 * Outputs the selected content option if any
 *
 * @author Nick Croft
 * @since 0.1
 * @version 0.2
 * @param array $instance Values set in widget isntance
 */
function mec_do_post_content( $instance ) {
    if ( !empty( $instance['show_content'] ) ) {

        if ( $instance['show_content'] == 'excerpt' )
            the_excerpt();
        elseif ( $instance['show_content'] == 'content-limit' )
            the_content_limit( ( int ) $instance['content_limit'], esc_html( $instance['more_text'] ) );
        else
            the_content( esc_html( $instance['more_text'] ) );
        echo '</div> <!-- .col-md-10 -->
        </aside>';
    }
}