<?php

//* Force layout to have sidebar
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_content_sidebar' );

//* Replace default loop
remove_action( 'genesis_loop' , 'genesis_do_loop');
add_action( 'genesis_loop' , 'yvn_product_loop');

function yvn_product_loop() {
	global $post;
	
	$output = '';

	$output .= '<div class="col-md-12 pull-left">';
	$output .= '<article '.genesis_attr( 'entry' ).'>';
	$image_url = '';
	if (  have_posts(  )  ) : while (  have_posts(  )  ) : the_post(  );
	
		$image = get_the_post_thumbnail( $post->ID  , 'full', array('class'=> 'img-responsive', 'alt'	=> "", 'title'	=> "" ) );
		$image_url = wp_get_attachment_url(  get_post_thumbnail_id(  $post->ID  )  );

		$output .= '<header '.genesis_attr( 'entry-header' ).'></header><div '.genesis_attr( 'entry-content' ).'>';
		$output_slider = '';
		$output_slider .= '<div class="cycle-slideshow" data-cycle-fx=scrollHorz data-cycle-timeout=0 data-cycle-pager-template="" data-cycle-pager=".navigation" data-cycle-slides="> div.for-flexslider" >';
		$output_slider .= '<div class="col-md-12 for-flexslider">';
		$output_slider .= '<div class="flexslider" data-slider-color="'. get_field( 'default_color' ) . '"><ul class="slides">';
		if( $image ){
			$output_slider .= '<li data-thumb="'. $image_url .'"><img class="img-responsive" alt="placeholder image" src="' . $image_url . '" /></li>';
		}
		if( get_field('product_gallery') )
		{
			while( has_sub_field('product_gallery') )
			{
				$image_ur = get_sub_field('gallery_item');
				$output_slider .= '<li data-thumb="'. $image_url .'"><img class="img-responsive" alt="placeholder image" src="' . $image_url . '" /></li>';
			}
		}
		$output_slider .= '</ul></div></div>';

		if( get_field('add_more_colors') ) {
			while( has_sub_field('additional_galleries') ) {
				$output_slider .= '<div class="col-md-12 for-flexslider">';
				$output_slider .= '<div class="flexslider" data-slider-color="'. get_sub_field('gallery_color') . '"><ul class="slides">';
				while( has_sub_field('gallery_image') ) {
					$output_slider .= '<li data-thumb="'. get_sub_field('gallery_item') .'"><img class="img-responsive" alt="placeholder image" src="' . get_sub_field('gallery_item') . '" /></li>';
				}
				$output_slider .= '</ul></div></div>';
			}
		}
		$output_slider .= '</div>';
	endwhile; endif;
	
	$output .= $output_slider;

	$output .= '</article></div>';
	
	$output .= '<div class="col-md-12"><div class="col-md-6"><h1>About</h1>'.apply_filters( 'the_content', get_the_content() ).'</div></div>';

	echo $output;
}


//* Replace sidebar
remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
add_action( 'genesis_sidebar', 'yvn_product_sidebar' );

function yvn_product_sidebar() {
	global $post;
		
	$output = '';
	
	if (  have_posts(  )  ) : while (  have_posts(  )  ) : the_post(  );
		$output .= '<header '.genesis_attr( 'entry-header' ).'><h1 class="entry-title" itemprop="headline" >'.get_the_title().'</h1></header>';
	endwhile; endif; 
	$output .= '<div class="navigation"><span data-product-color="' . get_field( 'default_color' ) . '" class="color_chooser" style="background-color:' . get_field( 'default_color' ) . '"></span>';
	if( get_field('add_more_colors') ) {
		while( has_sub_field('additional_galleries') ) {
			$output .= '<span data-product-color="' . get_sub_field('gallery_color') . '" class="color_chooser" style="background-color:' . get_sub_field('gallery_color') . '"></span>';
		}
	}
	$output .= '</div>';
	genesis_structural_wrap( 'sidebar' );
	do_action( 'genesis_before_sidebar_widget_area' );
	echo $output;
	do_action( 'genesis_after_sidebar_widget_area' );
	genesis_structural_wrap( 'sidebar', 'close' );
}

genesis();