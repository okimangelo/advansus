<?php
/*
Template Name: Single - Service Custom Post Type
*/

//Move Post Title Location
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
add_action( 'services_cpt_post_title', 'genesis_do_post_title');

remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

add_action( 'genesis_entry_content', 'okimangelo_single_service_cpt_content' );

function okimangelo_single_service_cpt_content() {
            ?>
            <section class="col-md-6 no-padding">
			        <?php
                    do_action('services_cpt_post_title');
                    if(has_post_thumbnail()) { the_post_thumbnail('services-thumb-image',array('class'=>"img-responsive alignleft")); }
                    the_content();?>
            </section>
            <section class="col-md-6">
                <h5>Quick Inquiry</h5>
                <?php echo do_shortcode('[contact-form-7 id="66" title="Get In Touch"]');?>
            </section>

           <?php wp_reset_query();
}


genesis();

