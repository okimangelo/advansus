<?php
/*
Template Name: Page - About
*/

remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
add_action( 'genesis_entry_content', 'okimangelo_about_page_content' );

function okimangelo_about_page_content() { ?>
			<?php if(has_post_thumbnail()) { ?>
				<?php the_post_thumbnail('full',array('class'=>"img-responsive alignleft bordered  ")); ?>
			<?php }

            the_content();
    
            wp_reset_query();
}

//remove_action( 'genesis_after_entry', 'genesis_do_post_content' );

add_action( 'genesis_after_entry', 'okimangelo_about_page_after_content' );

function okimangelo_about_page_after_content() {
    ?>
            <div class="row bg-gray">
                <?php
                //Query Team Post Type
                $args = array(
                    'post_type' => 'Team_Members',
                );

                $ourTeam = new WP_Query($args);

                // The Team Loop
                    if ( $ourTeam->have_posts() ) : ?>
                        <section class="team-members col-md-12">
                            <a href="<?php the_permalink();?>" class="active"><h2 class="bg-orange-skew-right text-center">Our Team</h2></a>
                            <ul class="col-md-12 col-xs-12 list-unstyled">
                            <?php
                                while ( $ourTeam->have_posts() ) : $ourTeam->the_post();
                                $ourTeam->current_post%2 == 0? $alignment = "left": $alignment = "right";
                                ?>

                                        <li class="member-info clearfix">

                                               <?php
                                               $attr = array(
                                                   'class'	=> "img-responsive bordered pull-". $alignment,
                                               );
                                               the_post_thumbnail('member-thumb-image',$attr);
                                               echo '<h5 class="bg-gray-skew member-name pull-'. $alignment .'">'. get_the_title() .'</h5>';
                                               echo '<p class="col-md-9 member-information pull-'. $alignment . ' text-'. $alignment . '">'. get_the_content() .'</p>';
                                               ?>

                                        </li>
                                    <?php endwhile;
                                wp_reset_postdata(); ?>
                            </ul>
                        </section>
                        <?php endif; ?>
            </div> <!-- .row.bg-gray-->
            <div class="row no-margin">

                <?php
                // Our Services Loop
                $args = array(
                    'post_type' => 'our_services',
                );

                $ourServices = new WP_Query($args);
                if ( $ourServices->have_posts() ) : ?>
                    <section class="our-services col-md-12">
                        <a href="<?php the_permalink();?>" class="active"><h2 class="bg-orange-skew-right text-center">Our Services</h2></a>
                        <ul class="col-md-12 col-xs-12 list-unstyled">
                            <?php while ( $ourServices->have_posts() ) : $ourServices->the_post(); ?>
                                <li class="service-link">
                                    <span class="glyphicon glyphicon-ok"></span>
                                    <?php echo '<a href="'.get_the_permalink().'">'.get_the_title().'</a>'; ?>
                                </li>
                            <?php endwhile;
                            wp_reset_postdata(); ?>
                        </ul>
                    </section>
                <?php endif; ?>
            </div> <!-- .row-->

    <div class="row no-margin">

        <?php
        // Our Services Loop
        $args = array(
            'post_type' => 'clients',
        );

        $clients = new WP_Query($args);
        if ( $clients->have_posts() ) : ?>
            <section class="our-clients col-md-12">
                <a href="<?php the_permalink();?>" class="active"><h2 class="bg-orange-skew-right text-center">Our Clients</h2></a>
                <aside id="ImagesCarousel" class="carousel slide">
                    <!-- carousel items -->
                    <div class="carousel-inner">
                        <ul class="list-unstyled">
                            <?php while ( $clients->have_posts() ) : $clients->the_post(); ?>
                                <li class="col-md-3 client-link col-xs-12">
                                    <?php echo '<a href="'.get_the_permalink().'">'.get_the_post_thumbnail().'</a>'; ?>
                                </li>
                            <?php endwhile;
                            wp_reset_postdata(); ?>
                        </ul>
                    </div>
                 </aside>
            </section>
        <?php endif; ?>
    </div> <!-- .row-->


<?php }

genesis();

