<?php
/*
Template Name: Page - Testimonials
*/

remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
add_action( 'genesis_entry_content', 'okimangelo_testimonials_page_content' );

function okimangelo_testimonials_page_content() {

    //Query All Testimonials Custom Post Type
    $args = array(
    'post_type' => 'testimonials-widget',
    );

    $testimonials = new WP_Query($args);

    // The Team Loop
    if ( $testimonials->have_posts() ) : ?>
        <ul class="testimonials-list list-unstyled">
            <?php
            while ( $testimonials->have_posts() ) : $testimonials->the_post();
                $testimonials->current_post%2 == 0? $alignment = "pull-left": $alignment = "pull-right";
                $TestimonialsID = get_the_ID()
                ?>

                <li class="testimonial-box clearfix col-md-6 no-padding <?php echo $alignment;?>">
                    <aside class="clearfix" itemscope="" itemtype="http://schema.org/Review">
                        <div class="testimonial-container">
                             <blockquote><?php the_content();?></blockquote>
                        </div>
                        <div class="credit"><span class="author">
                                <a class="pull-left" href="<?php echo get_post_meta( $TestimonialsID, 'testimonials-widget-url', true );?>" rel="nofollow" target="_blank"><?php the_title();?></a>
                                <p class="company-name pull-left"><?php echo get_post_meta( $TestimonialsID, 'testimonials-widget-company', true );?></p>
                            </span></div>
                        <div style="display: none;">
                            <div itemprop="author" itemscope="" itemtype="http://schema.org/Person"><meta itemprop="name" content="<?php the_title();?>">
                                <meta itemprop="url" content="<?php echo get_post_meta( $TestimonialsID, 'testimonials-widget-url', true );?>">
                            </div>
                            <meta itemprop="reviewBody" content="<?php the_content();?>">
                            <meta itemprop="datePublished" content="<?php the_date('Y-m-d', '', ''); ?>">
                            <meta itemprop="dateModified" content="<?php echo get_the_modified_date('Y-m-d'); ?>">
                            <meta itemprop="name" content="<?php the_content();?>">
                            <meta itemprop="url" content="<?php echo get_post_meta( $TestimonialsID, 'testimonials-widget-url', true );?>">

                            <div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating"><meta itemprop="reviewCount" content="1">
                            </div>
                            <div itemprop="itemReviewed" itemscope="" itemtype="http://schema.org/Thing"><meta itemprop="name" content="LOGO">
                                <meta itemprop="url" content="<?php the_permalink();?>">
                            </div>
                        </div>
                    </aside>
                </li>
            <?php endwhile;
            wp_reset_postdata(); ?>
        </ul>

    <?php endif;
            wp_reset_query();
}

genesis();

